package steps;

import cucumber.api.java8.En;
import org.openqa.selenium.By;

import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static org.junit.Assert.assertEquals;

public class TitleSearch implements En {
private int count =0;
    public TitleSearch( ) {
//        System.setProperty("webdriver.chrome.driver", "C:\\Users\\dbriskin\\WORK\\chromedriver\\chromedriver.exe");
//        System.setProperty("selenide.browser", "Chrome");
//        // -Dselenide.headless=true
//        Configuration.headless = false;
//        getWebDriver().manage().timeouts().implicitlyWait(500, TimeUnit.MILLISECONDS);
//        new WebDriverWait(firefoxDriver, pageLoadTimeout).until(
//                webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
//        System.setProperty("webdriver.gecko.driver", "C:\\Users\\dbriskin\\WORK\\geckodriver\\geckodriver.exe");

//        System.setProperty("selenide.browser", "Chrome");
//        System.setProperty("selenide.browser", "firefox");

        When("^I search for title tag in the page$", () -> {
            getWebDriver().findElements(By.tagName("title")).forEach(str -> {
                String title = str.getText();
                if (title == null || title.length() == 0 || title.trim().isEmpty()) {
                    count++;
                }
            } );


        });
        Then("^no empty title was found$", () -> {
            assertEquals(1,count);
        });
    }
}
