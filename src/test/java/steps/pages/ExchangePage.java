package steps.pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.sleep;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static junit.framework.TestCase.fail;

/**
 * @author Danny Briskin (DBriskin@qaconsultants.com)
 */
public class ExchangePage {
    public static Logger logger = LogManager.getLogger("com.qaconsultants.core");


    @FindBy(id = "currency-have-amount")
    private WebElement firstCurrencyInput;

    @FindBy(id = "currency-want-amount")
    private WebElement secondCurrencyInput;

    @FindBy(className = "currency-select")
    private List<WebElement> currencyComboBoxes;

    @FindBy(className = "cash-switch")
    private WebElement currencySwapButton;

    @FindBy(id = "currency-have-unit")
    private WebElement firstCurrencySelect;

    @FindBy(id = "currency-want-unit")
    private WebElement secondCurrencySelect;

    @FindBy(id = "locator_q")
    private WebElement zipCodeInput;

    @FindBy(id = "radiobranch")
    private WebElement radioBranch;

    @FindBy(id = "radioatm")
    private WebElement radioATM;


    public ExchangePage() {
        PageFactory.initElements(getWebDriver(), this);
    }

    /**
     * Because both comboboxes look the same and no id is given we retrieve them via index:
     *
     * @param comboBoxIndex index of the combobox 0 - for first currency, 1 - for second currency
     * @return a ComboBox
     */
    public WebElement findCurrencyComboBox(int comboBoxIndex) {
        return currencyComboBoxes.get(comboBoxIndex);
    }


    /**
     * Items in both comboboxes look the same
     *
     * @param itemName  a currency 3-letters code
     * @param itemOrder 0 - for first currency, 1 - for second currency
     * @return a combobox item
     */
    public WebElement findCurrencyComboBoxItem(String itemName, int itemOrder) {
        return $$(By.xpath("//div[@class='item'][@data-value='" + itemName + "']")).get(itemOrder);

    }

    /**
     * Enter value of first currency. Clear current input and place a new value
     *
     * @param webElement an input where to enter
     * @param value      currency value
     */
    public void enterCurrencyValue(WebElement webElement, Float value) {
        webElement.clear();
        webElement.sendKeys(Keys.chord(Keys.CONTROL, "a"));
        webElement.sendKeys(Keys.BACK_SPACE);
        webElement.sendKeys(value.toString());
        pressEnter(webElement);
    }

    /**
     * Get exchanged currency value as float
     *
     * @param webElement input where to get a value
     * @return float value of input
     */
    public float getExchangedValue(WebElement webElement) {
        return Float.parseFloat(webElement.getAttribute("value")
                .replaceAll("[^\\d.-]", ""));
    }

    /**
     * Clicks on element if it is available
     *
     * @param webElement an element to click
     */
    public void clickOnElement(WebElement webElement) {
        if (webElement.isEnabled() && webElement.isDisplayed()) {
            webElement.click();
        }
    }

    public void enterFirstCurrencyValue(Float firstCurrencyValue) {
        logger.info("Entering first currency value: " + firstCurrencyValue);
        this.enterCurrencyValue(
                this.firstCurrencyInput
                , firstCurrencyValue);
    }

    public void chooseFirstCurrency(String firstCurrencyCode) {
        this.chooseCurrency(firstCurrencyCode, 0);
    }

    public void chooseSecondCurrency(String secondCurrencyCode) {
        this.chooseCurrency(secondCurrencyCode, 1);
    }

    private void chooseCurrency(String currencyCode, int comboBoxIndex) {
        logger.info("Choosing " + (comboBoxIndex + 1) + " currency : " + currencyCode);
        this.clickOnElement(
                this.findCurrencyComboBox(comboBoxIndex));
        sleep(500);

        if (isCurrencyExchanged(currencyCode, comboBoxIndex)) {
            this.clickOnElement(this.findCurrencyComboBoxItem(currencyCode, comboBoxIndex));
        } else {
            logger.warn("Sorry. We don't exchange " + currencyCode + " currency!");
            fail(currencyCode + " is not exchanged");
        }

    }

    public Float getComputedResult() {
        sleep(1000);
        float computedResult = this.getExchangedValue(
                this.secondCurrencyInput);
        logger.info("Obtaining second currency value: " + computedResult);
        return computedResult;
    }

    public boolean isCurrencyExchanged(String currencyCode, int comboBoxIndex) {
        return $$(By.xpath("//div[@class='item'][@data-value='" + currencyCode + "']")).size() >= comboBoxIndex+1;
    }


    public void clickOnSwapCurrenciesButton() {
        logger.info("Swapping currency button");
        currencySwapButton.click();
    }

    public String getFirstComboBoxValue() {
        return firstCurrencySelect.getAttribute("value");
    }

    public String getSecondComboBoxValue() {
        return secondCurrencySelect.getAttribute("value");
    }

    public void enterSearchZipCode(String zipCode) {
        zipCodeInput.clear();
        zipCodeInput.sendKeys(Keys.chord(Keys.CONTROL, "a"));
        zipCodeInput.sendKeys(Keys.BACK_SPACE);
        zipCodeInput.sendKeys(zipCode);
    }

    private void pressEnter(WebElement webElement) {
        webElement.sendKeys(Keys.ENTER);
    }

    public void pressEnterOnSearch() {
        pressEnter(zipCodeInput);
    }

    public void chooseRadioBranch() {
        JavascriptExecutor jse = (JavascriptExecutor) getWebDriver();
        jse.executeScript("arguments[0].click()", radioBranch);
    }

    public void chooseRadioAtm() {
        JavascriptExecutor jse = (JavascriptExecutor) getWebDriver();
        jse.executeScript("arguments[0].click()", radioATM);
    }

}
