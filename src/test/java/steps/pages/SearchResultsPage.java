package steps.pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

/**
 * @author Danny Briskin (DBriskin@qaconsultants.com)
 */
public class SearchResultsPage {
    public static Logger logger = LogManager.getLogger("com.qaconsultants.core");

    @FindBy(id = "btnNextPane")
    private WebElement btnNextPane;

    public SearchResultsPage() {
        PageFactory.initElements(getWebDriver(), this);
    }

    public void goNextPane() {
        this.btnNextPane.click();
    }
}
