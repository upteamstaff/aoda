package steps;

import com.codeborne.selenide.Configuration;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static com.codeborne.selenide.WebDriverRunner.closeWebDriver;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class Hooks {
    @Before
    public void setup() {
        Logger logger = LogManager.getLogger("com.qaconsultants.core");
        try {
            String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
            String appConfigPath = rootPath + "application.properties";
            Properties appProps = new Properties();
            appProps.load(new FileInputStream(appConfigPath));

            System.setProperty(appProps.getProperty("webdriver.name", "webdriver.gecko.driver")
                    , appProps.getProperty("webdriver.path", "C:\\Users\\dbriskin\\WORK\\geckodriver\\geckodriver.exe"));

            System.setProperty(appProps.getProperty("selenide.name", "selenide.browser")
                    , appProps.getProperty("selenide.browser.name", "firefox"));

            Configuration.headless = Boolean.getBoolean(appProps.getProperty("selenide.browser.headless"));

//            getWebDriver().manage().timeouts().implicitlyWait(
//                    Integer.parseInt(appProps.getProperty("webdriver.implicitlyWait.time"))
//                    , TimeUnit.valueOf(appProps.getProperty("webdriver.implicitlyWait.unit"))
//            );
        } catch (IOException e) {
            logger.catching(e);
        }
    }

    @After
    public void tearDown() {
        getWebDriver().quit();
        closeWebDriver();
    }

//    @AfterClass
//    public static void tearDownAll() {
//         getWebDriver().quit();
//    }
}
