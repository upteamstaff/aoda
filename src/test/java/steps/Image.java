package steps;

public class Image {
    private String page;
    private String src;
    private String id;

    public Image(String page, String src, String id) {
        this.page = page;
        this.src = src;
        this.id = id;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "\nImage{" +
                "page='" + page + '\'' +
                ", src='" + src + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}
