package steps;

import cucumber.api.java8.En;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static com.codeborne.selenide.WebDriverRunner.url;
import static org.junit.Assert.assertEquals;

public class ImagesAlts implements En {
    private Set<Image> imagesWoAlts = new HashSet<>();

    public ImagesAlts() {
//        System.setProperty("webdriver.chrome.driver", "C:\\Users\\dbriskin\\WORK\\chromedriver\\chromedriver.exe");
//        System.setProperty("selenide.browser", "Chrome");
//        // -Dselenide.headless=true
//        Configuration.headless = false;
//        getWebDriver().manage().timeouts().implicitlyWait(500, TimeUnit.MILLISECONDS);

        When("^I search for all images tags in the page$", () -> {

            List<WebElement> allImages = getWebDriver().findElements(By.tagName("img"));
            for (WebElement imageFromList : allImages) {
                String imageUrl = imageFromList.getAttribute("alt");
                if (imageUrl == null || imageUrl.length() == 0 || imageUrl.trim().isEmpty()) {

                    imagesWoAlts.add(new Image(url(), imageUrl,imageFromList.getAttribute("id")));
//                    System.out.println("IMG_wo_ALT src " + imageFromList.getAttribute("src"));
                }

            }
            $$(By.tagName("a")).forEach(link -> {
                try {
                    if (link.exists() && link.isEnabled() && link.isDisplayed()) {
                        String nextUrl = link.getAttribute("href");
                        if (nextUrl != null
                                && nextUrl.startsWith("http")
                                && nextUrl.contains("qaconsultants.com")
                                && !nextUrl.equals("https://qaconsultants.com/")
                                && !nextUrl.equals("https://qaconsultants.com/#")) {
                            //   System.out.println("Going to: " + nextUrl);
                            link.click();
                            $$(By.tagName("img")).forEach(imageFromList -> {
                                String imageUrl = imageFromList.getAttribute("src");
                                String imageAlt = imageFromList.getAttribute("alt");
                                if (
                                        (imageAlt == null
                                                || imageAlt.length() == 0
                                                || imageAlt.trim().isEmpty()
                                        ) &&
                                        !imageUrl.contains("maps.gstatic.com")
                                                && !imageUrl.contains("maps.google.com")) {
//                                    count++;
//                                    imagesWoAlts.add(imageUrl);
                                    imagesWoAlts.add(new Image(nextUrl, imageUrl,imageFromList.getAttribute("id")));
//                                    System.out.println("IMG_wo_ALT src "
//                                            + imageFromList.getAttribute("src") + " on page  " + nextUrl);

                                }
                            });

                        }
                    }
                } catch (WebDriverException e) {
                    System.out.println("AAAA");
                }
            });

        });
        Then("^no such image was was found$", () -> {
            System.out.println(imagesWoAlts);
            assertEquals(0, imagesWoAlts.size());
        });
    }
}
