package steps;

import cucumber.api.java8.En;
import org.junit.Assert;
import steps.pages.ExchangePage;
import steps.pages.SearchResultsPage;

import static com.codeborne.selenide.CollectionCondition.sizeGreaterThan;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.sleep;
import static com.codeborne.selenide.WebDriverRunner.url;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class RbcExchange implements En {
    private static ExchangePage exchangePage;
    private static SearchResultsPage searchResultsPage;

    public RbcExchange() {
        Given("^the web page \"([^\"]*)\" is displayed\\.$", RbcExchange::openSite);

        When("^I choose : \"([^\"]*)\" as from currency$", (String firstCurrencyCode) -> {
            exchangePage.chooseFirstCurrency(firstCurrencyCode);
        });

        And("^I enter first currency value: (.+)$", (Float firstCurrencyValue) -> {
            exchangePage.enterFirstCurrencyValue(firstCurrencyValue);
        });

        And("^I choose : \"([^\"]*)\" as to currency$", (String secondCurrencyCode) -> {
            exchangePage.chooseSecondCurrency(secondCurrencyCode);
        });

        Then("^I get second value: (.+)$", (Float expectedResult) -> {
            assertEquals(expectedResult, exchangePage.getComputedResult());
        });
        And("^I press the swap button$", () -> {
            exchangePage.clickOnSwapCurrenciesButton();
        });
        Then("^I get : \"([^\"]*)\" as from currency and : \"([^\"]*)\" as to currency$",
                (String firstCurrencyCode, String secondCurrencyCode) -> {
                    assertEquals(firstCurrencyCode, exchangePage.getFirstComboBoxValue());
                    assertEquals(secondCurrencyCode, exchangePage.getSecondComboBoxValue());
                });
        When("^I put : \"([^\"]*)\" zip code into search box$", (String zipCode) -> {
            exchangePage.enterSearchZipCode(zipCode);
        });

        And("^choose : \"([^\"]*)\" radiobutton$", (String arg0) -> {
            exchangePage.chooseRadioBranch();
        });
        And("^press search button$", () -> {
            exchangePage.pressEnterOnSearch();
        });
        Then("^I see : \"([^\"]*)\" page$", (String siteUrl) -> {
            sleep(3500);
            $$("a").shouldHave(sizeGreaterThan(2));
            Assert.assertEquals(url(), siteUrl);

            searchResultsPage=new SearchResultsPage();
        });
    }

    private static void openSite(String siteUrl) {
        open(siteUrl);
        $$("a").shouldHave(sizeGreaterThan(2));
        exchangePage = new ExchangePage();
        Assert.assertEquals(url(), siteUrl);
    }
}
