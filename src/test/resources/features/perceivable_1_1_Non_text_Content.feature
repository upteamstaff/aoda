@alltests
@nontextcontent

Feature: Non-text Content
  All non-text content that is presented to the user has a text alternative
  that serves the equivalent purpose, except for the situations listed below
  Level A

  Scenario: All images have a ALT attribute
    Given the web page "https://qaconsultants.com/" is displayed.
    When I search for all images tags in the page
    Then no such image was was found