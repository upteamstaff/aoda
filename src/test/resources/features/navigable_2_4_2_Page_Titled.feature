@alltests
@title

Feature: Pages are Titled
  Web pages have titles that describe topic or purpose
  Level A

  Scenario: Accessibility test on the specified web page for having a title
    Given the web page "https://qaconsultants.com/" is displayed.
    When I search for title tag in the page
    Then no empty title was found