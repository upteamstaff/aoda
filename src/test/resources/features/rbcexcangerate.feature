@alltests
@rbcexchange

Feature: RBC bank exchange

  Scenario Outline: Exchange one currency to another
    Given the web page "https://online.royalbank.com/cgi-bin/tools/foreign-exchange-calculator/start.cgi" is displayed.
    When I choose : "<first>" as from currency
    And I choose : "<second>" as to currency
    And I enter first currency value: <F>
    Then I get second value: <S>
    Examples:
      | first | second | F   | S      |
      | EUR   | SGD    | 100 | 148.06 |
      | AUD   | EUR    | 100 | 60.39  |

  Scenario: Swap currencies
    Given the web page "https://online.royalbank.com/cgi-bin/tools/foreign-exchange-calculator/start.cgi" is displayed.
    When I choose : "EUR" as from currency
    And I choose : "GBP" as to currency
    And I press the swap button
    Then I get : "GBP" as from currency and : "EUR" as to currency

  Scenario: Locate RBC branch
    Given the web page "https://online.royalbank.com/cgi-bin/tools/foreign-exchange-calculator/start.cgi" is displayed.
    When I put : "L4J3W3" zip code into search box
    And choose : "Branch" radiobutton
    And press search button
    Then I see : "https://maps.rbcroyalbank.com/locator/searchResults.php" page